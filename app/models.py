from django.db import models


class Category(models.Model):
    name = models.CharField(verbose_name='Category', max_length=200, db_index=True)
    slug = models.SlugField(verbose_name='Slug', max_length=200, db_index=True, unique=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    category = models.ForeignKey(Category, verbose_name='Category', related_name='products', on_delete=models.PROTECT)
    name = models.CharField(verbose_name='Name', max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, db_index=True)
    image = models.ImageField(verbose_name='Image', upload_to='products/%Y/%m/%d/', blank=True)
    description = models.TextField(verbose_name='Description', blank=True)
    price = models.DecimalField(verbose_name='Price', max_digits=10, decimal_places=2)
    stock = models.PositiveIntegerField(verbose_name='In stock')
    available = models.BooleanField(verbose_name='Available', default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Client(models.Model):
    first_name = models.CharField(verbose_name='First name', max_length=50)
    last_name = models.CharField(verbose_name='Last name', max_length=50)
    birthday = models.DateField(verbose_name='Birthday', blank=True)
    email = models.EmailField(verbose_name='Email')
    phone = models.CharField(verbose_name='Phone', max_length=20)
    city = models.CharField(verbose_name='City', max_length=50)
    postal_code = models.CharField(verbose_name='Postal code', max_length=10)
    address = models.CharField(verbose_name='Address', max_length=250)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.last_name) + ' '.format(self.first_name)


class Order(models.Model):
    client = models.ForeignKey(Client, null=True, on_delete=models.PROTECT)
    created = models.DateTimeField(verbose_name='Created', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Updated', auto_now=True)
    paid = models.BooleanField(verbose_name='Paid', default=False)

    def __str__(self):
        return self.id

    def get_order_cost(self):
        return sum(item.get_item_cost() for item in self.items.all())


class OrderItem(models.Model):
    order = models.ForeignKey(Order, related_name='items', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, related_name='order_items', on_delete=models.CASCADE)
    price = models.DecimalField(verbose_name='Price', max_digits=10, decimal_places=2)
    quantity = models.PositiveIntegerField(verbose_name='Quantity', default=1)

    def __str__(self):
        return self.id

    def get_item_cost(self):
        return self.price * self.quantity
