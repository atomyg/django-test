from django.shortcuts import render, get_object_or_404
from .models import Category, Product


def product_list(request, category_slug=None):
    category = None
    categories = Category.objects.all()
    products = Product.objects.all(available=True)
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        products = Product.filter(category=category)
    return render(request, 'app/product/list.html', {
        'category': category,
        'categories': categories,
        'products': products
    })


def product_detail(request, id, slug):
    product = get_object_or_404(Product, id=id, slug=slug, available=True)
    return render(request, 'app/product/detail.html', {'product': product})
